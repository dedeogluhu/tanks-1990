from PIL import Image


def change_size(w, h):
    img = Image.open('sprites/tank.png')
    new = img.resize((w, h))
    new.save('sprites/new_tank.png')


def change_bullet_size(w, h):
    img = Image.open('sprites/bullet.png')
    new = img.resize((w, h))
    new.save('sprites/bullet.png')

change_bullet_size(55, 55)