import pygame

from image_prepare import change_size

change_size(55, 55)


KEYS = [pygame.K_UP, pygame.K_DOWN, pygame.K_RIGHT, pygame.K_LEFT]
pygame.init()

font = pygame.font.Font('freesansbold.ttf', 24)

screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption('Tanks 1990')
clock = pygame.time.Clock()

# Player image
player_img = pygame.image.load("sprites/new_tank.png")
player_x = 370
player_y = 370
player_x_change = 0
player_y_change = 0
player_life = 3

# bullet image
bullet_img = pygame.image.load("sprites/bullet.png")
bullet_x = player_x
bullet_y = player_y
bullet_x_change = 0
bullet_y_change = 10
bullet_state = 'ready'


def player(x, y):
    screen.blit(player_img, (x, y))


def fire_bullet(x, y):
    global bullet_state
    bullet_state = 'fire'
    screen.blit(bullet_img, (x, y))


def show_life(x, y):
    score = font.render(f"Life: {str(player_life)}", True, (0, 0, 0))
    screen.blit(score, (x, y))


running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                player_x_change = -5
            if event.key == pygame.K_RIGHT:
                player_x_change = 5
            if event.key == pygame.K_UP:
                player_y_change = -5
            if event.key == pygame.K_DOWN:
                player_y_change = 5
            if event.key == pygame.K_SPACE:
                fire_bullet(bullet_x, bullet_y)

        if event.type == pygame.KEYUP:
            if event.key in KEYS:
                player_x_change = player_y_change = 0

    if player_x <= 0:
        player_x = 0
    elif player_y <= 0:
        player_y = 0
    elif player_x >= 745:
        player_x = 745
    elif player_y >= 545:
        player_y = 545

    if bullet_y <= 0:
        bullet_y = player_y
        bullet_x = player_x
        bullet_state = 'ready'

    screen.fill((255, 255, 255))
    player_x += player_x_change
    player_y += player_y_change

    if bullet_state == "fire":
        fire_bullet(bullet_x, bullet_y)
        bullet_y -= bullet_y_change

    player(player_x, player_y)
    show_life(10, 10)
    pygame.display.update()
    clock.tick(30)